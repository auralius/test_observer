%% test_observer.m 
% A first order differential equation that will converge to a target value.
% Consider:
% 
% $\dot{z} = g(x) - z$
%
% As $t>0$, $z$ will converge to $g(x)$
% 


%% Initialization
clear all;
clc;

Ts = 0.001;
t = 0 : Ts : 10;

%% Converging target and rate
k = 2; % how fast we converge
target = 2; % converging target

%% Solving the ODE
z(1) = 0;
for index = 1 : length(t) 
    z_dot(index) = k*(target - z(index));
    z(index + 1) = z(index) + z_dot(index) * Ts;
end

%% Result
plot(t, z(1:length(t)))
