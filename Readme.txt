A first order differential equation that will converge to a target value.
Consider:

$\dot{z} = g(x) - z$

As $t>0$, $z$ will converge to $g(x)$
 
